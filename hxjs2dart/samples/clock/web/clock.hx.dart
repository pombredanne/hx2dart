import 'dart:html';
import 'dart:math';
import 'dart:async';
main()=>Clock.main();
class Ball
{
	
	Ball( root,x,y,color ) {
		this.root = root;
		this.x = x;
		this.y = y;
		this.elem = document.createElement("img");
		this.elem.src = Balls.PNGS[color];
		Clock.makeAbsolute(this.elem);
		Clock.setElementPosition(this.elem, x, y);
		root.append(this.elem);
		this.ax = 0.0;
		this.ay = GRAVITY;
		this.vx = randomVelocity();
		this.vy = randomVelocity();
	}
	static var GRAVITY = 400.0;
	static var RESTITUTION = 0.8;
	static var INIT_VELOCITY = 800.0;
	static var RADIUS = 14.0;
	static randomVelocity (  ) {
		return (new Random().nextDouble() - 0.5) * INIT_VELOCITY;
	}
	tick( delta ) {
		this.vx += this.ax * delta;
		this.vy += this.ay * delta;
		this.x += this.vx * delta;
		this.y += this.vy * delta;
		if((this.x < RADIUS || this.x > Balls.get_clientWidth())) {
			this.elem.remove();
			return false;
		}  ;
		if((this.y > Balls.get_clientHeight())) {
			this.y = Balls.get_clientHeight();
			this.vy *= -RESTITUTION;
		}  ;
		Clock.setElementPosition(this.elem, this.x - RADIUS, this.y - RADIUS);
		return true;
	}
	
	var ay;
	var ax;
	var vy;
	var vx;
	var y;
	var x;
	var elem;
	var root;
	
}

class Balls
{
	
	Balls(  ) {
		this.lastTime = new DateTime.now().millisecondsSinceEpoch;
		this.balls = [];
		this.root = document.createElement("div");
		document.body.append(this.root);
		Clock.makeAbsolute(this.root);
		Clock.setElementSize(this.root, 0.0, 0.0, 0.0, 0.0);
	}
	static get_clientWidth (  ) {
		return window.innerWidth;
	}
	static get_clientHeight (  ) {
		return window.innerHeight;
	}
	static var RADIUS2 = Ball.RADIUS * Ball.RADIUS;
	static var GREEN_BALL_INDEX = 1;
	static var BLUE_BALL_INDEX = 2;
	static var DK_GRAY_BALL_INDEX = 4;
	static var RED_BALL_INDEX = 5;
	static var PNGS = ["images/ball-d9d9d9.png", "images/ball-009a49.png", "images/ball-13acfa.png", "images/ball-265897.png", "images/ball-b6b4b5.png", "images/ball-c0000b.png", "images/ball-c9c9c9.png"];
	add( x,y,color ) {
		this.balls.add(new Ball(this.root, x, y, color));
	}
	
	newDistanceSquared( delta,b0,b1 ) {
		var nb0x = b0.x + b0.vx * delta;
		var nb0y = b0.y + b0.vy * delta;
		var nb1x = b1.x + b1.vx * delta;
		var nb1y = b1.y + b1.vy * delta;
		var ndx = (nb0x - nb1x).abs();
		var ndy = (nb0y - nb1y).abs();
		var nd2 = ndx * ndx + ndy * ndy;
		return nd2;
	}
	
	collideBalls( delta ) {
		var _g = this;
		this.balls.forEach(( b0 ) {
			_g.balls.forEach(( b1 ) {
				var dx = (b0.x - b1.x).abs();
				var dy = (b0.y - b1.y).abs();
				var d2 = dx * dx + dy * dy;
				if((d2 < RADIUS2)) {
					if((_g.newDistanceSquared(delta, b0, b1) > d2)) return;  ;
					var d = sqrt(d2);
					if((d == 0)) return;  ;
					dx /= d;
					dy /= d;
					var impactx = b0.vx - b1.vx;
					var impacty = b0.vy - b1.vy;
					var impactSpeed = impactx * dx + impacty * dy;
					b0.vx -= dx * impactSpeed;
					b0.vy -= dy * impactSpeed;
					b1.vx += dx * impactSpeed;
					b1.vy += dy * impactSpeed;
				}  ;
			});
		});
	}
	
	tick( now ) {
		Clock.showFps(1000.0 / (now - this.lastTime + 0.01));
		var delta = min((now - this.lastTime) / 1000.0, 0.1);
		this.lastTime = now;
		this.balls = this.balls.where(( ball ) {
			return ball.tick(delta);
		}).toList();
		this.collideBalls(delta);
	}
	
	var balls;
	var lastTime;
	var root;
	
}

class Clock
{
	static var fpsAverage;
	static main (  ) {
		new CountDownClock();
	}
	static showFps ( fps ) {
		if((fpsAverage == null)) fpsAverage = fps;  ;
		fpsAverage = fps * 0.05 + fpsAverage * 0.95;
		query("#" + "notes").text = (fpsAverage).round().toString() + " fps";
	}
	static makeAbsolute ( elem ) {
		elem.style.left = "0px";
		elem.style.top = "0px";
		elem.style.position = "absolute";
	}
	static makeRelative ( elem ) {
		elem.style.position = "relative";
	}
	static setElementPosition ( elem,x,y ) {
		elem.style.transform = "translate(" + x.toString() + "px, " + y.toString() + "px)";
	}
	static setElementSize ( elem,l,t,r,b ) {
		setElementPosition(elem, l, t);
		elem.style.right = "${r}px";
		elem.style.bottom = "${b}px";
	}
	
}

class CountDownClock
{
	
	CountDownClock(  ) {
		this.hours = [null, null];
		this.minutes = [null, null];
		this.seconds = [null, null];
		this.displayedHour = this.displayedMinute = this.displayedSecond = -1;
		this.balls = new Balls();
		var parent = query("#" + "canvas-content");
		this.createNumbers(parent, parent.clientWidth, parent.clientHeight);
		this.updateTime(new DateTime.now());
		window.requestAnimationFrame(this.tick);
	}
	static var NUMBER_SPACING = 19.0;
	static var BALL_WIDTH = 19.0;
	static var BALL_HEIGHT = 19.0;
	createNumbers( parent,width,height ) {
		var root = document.createElement("div");
		Clock.makeRelative(root);
		root.style.textAlign = "center";
		query("#" + "canvas-content").append(root);
		var hSize = (BALL_WIDTH * ClockNumber.WIDTH + NUMBER_SPACING) * 6 + (BALL_WIDTH + NUMBER_SPACING) * 2;
		hSize -= NUMBER_SPACING;
		var vSize = BALL_HEIGHT * ClockNumber.HEIGHT;
		var x = (width - hSize) / 2;
		var y = (height - vSize) / 3;
		{
			var _g1 = 0, _g = this.hours.length;
			while((_g1 < _g)) {
				var i = _g1++;
				this.hours[i] = new ClockNumber(this, x, Balls.BLUE_BALL_INDEX);
				root.append(this.hours[i].root);
				Clock.setElementPosition(this.hours[i].root, x, y);
				x += BALL_WIDTH * ClockNumber.WIDTH + NUMBER_SPACING;
			};
		};
		root.append(new Colon(x, y).root);
		x += BALL_WIDTH + NUMBER_SPACING;
		{
			var _g1 = 0, _g = this.minutes.length;
			while((_g1 < _g)) {
				var i = _g1++;
				this.minutes[i] = new ClockNumber(this, x, Balls.RED_BALL_INDEX);
				root.append(this.minutes[i].root);
				Clock.setElementPosition(this.minutes[i].root, x, y);
				x += BALL_WIDTH * ClockNumber.WIDTH + NUMBER_SPACING;
			};
		};
		root.append(new Colon(x, y).root);
		x += BALL_WIDTH + NUMBER_SPACING;
		{
			var _g1 = 0, _g = this.seconds.length;
			while((_g1 < _g)) {
				var i = _g1++;
				this.seconds[i] = new ClockNumber(this, x, Balls.GREEN_BALL_INDEX);
				root.append(this.seconds[i].root);
				Clock.setElementPosition(this.seconds[i].root, x, y);
				x += BALL_WIDTH * ClockNumber.WIDTH + NUMBER_SPACING;
			};
		};
	}
	
	pad2( number ) {
		if((number < 10)) return "0${number}";  ;
		return "${number}";
	}
	
	setDigits( digits,numbers ) {
		var _g1 = 0, _g = numbers.length;
		while((_g1 < _g)) {
			var i = _g1++;
			var digit = digits.codeUnitAt(i) - "0".codeUnitAt(0);
			numbers[i].setPixels(ClockNumbers.PIXELS[digit]);
		};
	}
	
	updateTime( now ) {
		if((now.hour != this.displayedHour)) {
			this.setDigits(this.pad2(now.hour), this.hours);
			this.displayedHour = now.hour;
		}  ;
		if((now.minute != this.displayedMinute)) {
			this.setDigits(this.pad2(now.minute), this.minutes);
			this.displayedMinute = now.minute;
		}  ;
		if((now.second != this.displayedSecond)) {
			this.setDigits(this.pad2(now.second), this.seconds);
			this.displayedSecond = now.second;
		}  ;
	}
	
	tick( time ) {
		this.updateTime(new DateTime.now());
		this.balls.tick(time);
		window.requestAnimationFrame(this.tick);
	}
	
	var balls;
	var displayedSecond;
	var displayedMinute;
	var displayedHour;
	var seconds;
	var minutes;
	var hours;
	
}

class ClockNumber
{
	
	ClockNumber( app,pos,ballColor ) {
		this.app = app;
		this.ballColor = ballColor;
		this.imgs = [];
		{
			var _g1 = 0, _g = HEIGHT;
			while((_g1 < _g)) {
				var i = _g1++;
				this.imgs.add(null);
			};
		};
		this.root = document.createElement("div");
		Clock.makeAbsolute(this.root);
		Clock.setElementPosition(this.root, pos, 0.0);
		{
			var _g1 = 0, _g = HEIGHT;
			while((_g1 < _g)) {
				var y = _g1++;
				this.imgs[y] = [];
				{
					var _g3 = 0, _g2 = WIDTH;
					while((_g3 < _g2)) {
						var i = _g3++;
						this.imgs[y].add(null);
					};
				};
			};
		};
		{
			var _g1 = 0, _g = HEIGHT;
			while((_g1 < _g)) {
				var y = _g1++;
				{
					var _g3 = 0, _g2 = WIDTH;
					while((_g3 < _g2)) {
						var x = _g3++;
						this.imgs[y][x] = document.createElement("img");
						this.root.append(this.imgs[y][x]);
						Clock.makeAbsolute(this.imgs[y][x]);
						Clock.setElementPosition(this.imgs[y][x], x * CountDownClock.BALL_WIDTH, y * CountDownClock.BALL_HEIGHT);
					};
				};
			};
		};
	}
	static var WIDTH = 4;
	static var HEIGHT = 7;
	setPixels( px ) {
		var _g4 = this;
		{
			var _g1 = 0, _g = HEIGHT;
			while((_g1 < _g)) {
				var y = _g1++;
				{
					var _g3 = 0, _g2 = WIDTH;
					while((_g3 < _g2)) {
						var x = _g3++;
						var img = [this.imgs[y][x]];
						if((this.pixels != null)) {
							if((this.pixels[y][x] != 0 && px[y][x] == 0)) haxe_Timer.delay((( img ) {
								return (  ) {
									var r = img[0].getBoundingClientRect();
									var absx = r.left;
									var absy = r.top;
									_g4.app.balls.add(absx, absy, _g4.ballColor);
								};
							})(img), 0);  ;
						}  ;
						if((px[y][x] != 0)) img[0].src = Balls.PNGS[this.ballColor];  else img[0].src = Balls.PNGS[6];
					};
				};
			};
		};
		this.pixels = px;
	}
	
	var ballColor;
	var pixels;
	var imgs;
	var root;
	var app;
	
}

class Colon
{
	
	Colon( xpos,ypos ) {
		this.root = document.createElement("div");
		Clock.makeAbsolute(this.root);
		Clock.setElementPosition(this.root, xpos, ypos);
		var dot = document.createElement("img");
		dot.src = Balls.PNGS[Balls.DK_GRAY_BALL_INDEX];
		this.root.append(dot);
		Clock.makeAbsolute(dot);
		Clock.setElementPosition(dot, 0.0, 2.0 * CountDownClock.BALL_HEIGHT);
		dot = document.createElement("img");
		dot.src = Balls.PNGS[Balls.DK_GRAY_BALL_INDEX];
		this.root.append(dot);
		Clock.makeAbsolute(dot);
		Clock.setElementPosition(dot, 0.0, 4.0 * CountDownClock.BALL_HEIGHT);
	}
	var root;
	
}

class ClockNumbers
{
	static var PIXELS = [[[1, 1, 1, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 1, 1, 1]], [[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]], [[1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1], [1, 1, 1, 1], [1, 0, 0, 0], [1, 0, 0, 0], [1, 1, 1, 1]], [[1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1], [1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1], [1, 1, 1, 1]], [[1, 0, 0, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]], [[1, 1, 1, 1], [1, 0, 0, 0], [1, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1], [1, 1, 1, 1]], [[1, 1, 1, 1], [1, 0, 0, 0], [1, 0, 0, 0], [1, 1, 1, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 1, 1, 1]], [[1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]], [[1, 1, 1, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 1, 1, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 1, 1, 1]], [[1, 1, 1, 1], [1, 0, 0, 1], [1, 0, 0, 1], [1, 1, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1], [1, 1, 1, 1]]];
	
}

class haxe_Timer
{
	static delay ( f,time_ms ) {
		var duration = new Duration(milliseconds:time_ms);
		return new Timer(duration, f);
	}
	
}

