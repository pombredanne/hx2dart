import 'dart:html';
import 'dart:math';
import 'dart:async';
main()=>Main.main();
class Main
{
	static var notes;
	static var fpsAverage;
	static main (  ) {
		var canvas = query("#area");
		window.setImmediate(new SolarSystem(canvas).start);
		notes = query("#fps");
	}
	static showFps ( fps ) {
		if((fpsAverage == null)) fpsAverage = fps;  ;
		fpsAverage = fps * 0.05 + fpsAverage * 0.95;
		notes.text = (fpsAverage).round().toString() + " fps";
	}
	
}

class SolarSystem
{
	
	SolarSystem( canvas ) {
		this.canvas = canvas;
	}
	normalizePlanetSize( r ) {
		return log(r + 1) * (this.width / 100.0);
	}
	
	normalizeOrbitRadius( r ) {
		return r * (this.width / 10.0);
	}
	
	addAsteroidBelt( body,count ) {
		var _g = 0;
		while((_g < count)) {
			var i = _g++;
			var radius = 2.06 + new Random().nextDouble() * 1.21;
			body.addPlanet(new PlanetaryBody(this, "#777", 0.1 * new Random().nextDouble(), radius, radius * 2));
		};
	}
	
	requestRedraw(  ) {
		window.requestAnimationFrame(this.draw);
	}
	
	drawPlanets( context ) {
		this.sun.draw(context, new Point(this.width / 2, this.height / 2));
	}
	
	drawBackground( context ) {
		context.clearRect(0, 0, this.width, this.height);
	}
	
	draw( _ ) {
		var time = new DateTime.now().millisecondsSinceEpoch;
		if((this.renderTime != null)) Main.showFps(1000 / (time - this.renderTime));  ;
		this.renderTime = time;
		var context = this.canvas.context2D;
		this.drawBackground(context);
		this.drawPlanets(context);
		this.requestRedraw();
	}
	
	start(  ) {
		this.width = this.canvas.parent.clientWidth;
		this.height = this.canvas.parent.clientHeight;
		this.canvas.width = this.width.toInt();
		var mercury = new PlanetaryBody(this, "orange", 0.382, 0.387, 0.241);
		var venus = new PlanetaryBody(this, "green", 0.949, 0.723, 0.615);
		this.sun = new PlanetaryBody(this, "#ff2", 14.0);
		this.sun.addPlanet(mercury);
		this.sun.addPlanet(venus);
		var earth = new PlanetaryBody(this, "#33f", 1.0, 1.0, 1.0);
		var moon = new PlanetaryBody(this, "gray", 0.2, 0.14, 0.075);
		var mars = new PlanetaryBody(this, "red", 0.532, 1.524, 1.88);
		earth.addPlanet(moon);
		earth.addPlanet(mars);
		this.sun.addPlanet(earth);
		this.addAsteroidBelt(this.sun, 150);
		var f = 0.1;
		var h = 1 / 1500.0;
		var g = 1 / 72.0;
		var jupiter = new PlanetaryBody(this, "gray", 4.0, 5.203, 11.86);
		var io = new PlanetaryBody(this, "gray", 3.6 * f, 421 * h, 1.769 * g);
		var europa = new PlanetaryBody(this, "gray", 3.1 * f, 671 * h, 3.551 * g);
		var ganymede = new PlanetaryBody(this, "gray", 5.3 * f, 1070 * h, 7.154 * g);
		var callisto = new PlanetaryBody(this, "gray", 4.8 * f, 1882 * h, 16.689 * g);
		jupiter.addPlanet(io);
		jupiter.addPlanet(europa);
		jupiter.addPlanet(ganymede);
		jupiter.addPlanet(callisto);
		this.sun.addPlanet(jupiter);
		this.requestRedraw();
	}
	
	var renderTime;
	var sun;
	var height;
	var width;
	var canvas;
	
}

class PlanetaryBody
{
	
	PlanetaryBody( solarSystem,color,bodySize,[orbitRadius= 0.0,orbitPeriod= 0.0] ) {
		this.solarSystem = solarSystem;
		this.color = color;
		this.planets = [];
		this.bodySize = solarSystem.normalizePlanetSize(bodySize);
		this.orbitRadius = solarSystem.normalizeOrbitRadius(orbitRadius);
		this.orbitSpeed = this.calculateSpeed(orbitPeriod);
	}
	calculatePos( p ) {
		if((this.orbitSpeed == 0.0)) return p;  ;
		var angle = this.solarSystem.renderTime * this.orbitSpeed;
		return new Point(this.orbitRadius * cos(angle) + p.x, this.orbitRadius * sin(angle) + p.y);
	}
	
	calculateSpeed( period ) {
		if((period == 0)) return 0;  ;
		return 1 / (2880 * period);
	}
	
	drawChildren( context,p ) {
		var _g = 0, _g1 = this.planets;
		while((_g < _g1.length)) {
			var planet = _g1[_g];
			++_g;
			planet.draw(context, p);
		};
	}
	
	drawSelf( context,p ) {
		if((p.x + this.bodySize < 0 || p.x - this.bodySize >= context.canvas.width)) return;  ;
		if((p.y + this.bodySize < 0 || p.y - this.bodySize >= context.canvas.height)) return;  ;
		context.lineWidth = 0.5;
		context.fillStyle = this.color;
		context.strokeStyle = this.color;
		if((this.bodySize >= 2.0)) {
			context.shadowOffsetX = 2;
			context.shadowOffsetY = 2;
			context.shadowBlur = 2;
			context.shadowColor = "#ddd";
		}  ;
		context.beginPath();
		context.arc(p.x, p.y, this.bodySize, 0, PI * 2, false);
		context.fill();
		context.closePath();
		context.shadowOffsetX = 0;
		context.shadowOffsetY = 0;
		context.shadowBlur = 0;
		context.beginPath();
		context.arc(p.x, p.y, this.bodySize, 0, PI * 2, false);
		context.fill();
		context.closePath();
		context.stroke();
	}
	
	draw( context,p ) {
		var pos = this.calculatePos(p);
		this.drawSelf(context, pos);
		this.drawChildren(context, pos);
	}
	
	addPlanet( planet ) {
		this.planets.add(planet);
	}
	
	var planets;
	var orbitSpeed;
	var orbitRadius;
	var bodySize;
	var solarSystem;
	var color;
	
}

